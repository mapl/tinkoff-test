package com.mapl.myapplication.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.mapl.myapplication.data.Gif

@Database(entities = [Gif::class], version = 1)

abstract class AppDatabase : RoomDatabase() {
    abstract val dao: AppDao
}
